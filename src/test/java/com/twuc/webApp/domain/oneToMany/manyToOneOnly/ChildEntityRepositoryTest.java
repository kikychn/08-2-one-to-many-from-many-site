package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(em->{
            ParentEntity parent = parentEntityRepository.save(new ParentEntity("parent"));
            parentId.setValue(parent.getId());
            ChildEntity child = childEntityRepository.save(new ChildEntity("child"));
            childId.setValue(child.getId());
            child.setParent(parent);
        });

        run(em->{
            ChildEntity savedChild = childEntityRepository.findById(childId.getValue()).orElseThrow(NoSuchElementException::new);
            assertEquals("parent", savedChild.getParent().getName());
        });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(em->{
            ParentEntity parent = parentEntityRepository.save(new ParentEntity("parent"));
            parentId.setValue(parent.getId());
            ChildEntity child = childEntityRepository.save(new ChildEntity("child"));
            childId.setValue(child.getId());
            child.setParent(parent);
        });

        flushAndClear(em->{
            ParentEntity savedParent = parentEntityRepository
                    .findById(parentId.getValue())
                    .orElseThrow(NoSuchElementException::new);
            ChildEntity savedChild = childEntityRepository
                    .findById(childId.getValue())
                    .orElseThrow(NoSuchElementException::new);
            savedParent.removeChild(savedChild);
        });

        run(em->{
            ParentEntity savedParent = parentEntityRepository.findById(parentId.getValue()).orElseThrow(NoSuchElementException::new);
            ChildEntity savedChild = childEntityRepository.findById(childId.getValue()).orElseThrow(NoSuchElementException::new);
            assertNotNull(savedChild);
            assertNull(savedChild.getParent());
        });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(em->{
            ParentEntity parent = parentEntityRepository.save(new ParentEntity("parent"));
            parentId.setValue(parent.getId());
            ChildEntity child = childEntityRepository.save(new ChildEntity("child"));
            childId.setValue(child.getId());
            child.setParent(parent);
        });

        flushAndClear(em->{
            childEntityRepository.deleteById(childId.getValue());
            parentEntityRepository.deleteById(parentId.getValue());
        });

        run(em->{
            assertThrows(NoSuchElementException.class, () -> {
                parentEntityRepository.findById(parentId.getValue()).orElseThrow(NoSuchElementException::new);
            });
            assertThrows(NoSuchElementException.class, () -> {
                childEntityRepository.findById(childId.getValue()).orElseThrow(NoSuchElementException::new);
            });
        });
        // --end-->
    }
}